package com.efimchick.ifmo;


import com.efimchick.ifmo.util.CourseResult;
import com.efimchick.ifmo.util.Person;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Collecting {

    public int sum(IntStream intStream) {
        return intStream.sum();
    }

    public int production(IntStream intStream) {
        return intStream.reduce(1 , (acc, n) -> acc * n);
    }

    public int oddSum(IntStream intStream) {
        return intStream.filter(n -> Math.abs(n % 2) == 1).sum();
    }

    public Map<Integer, Integer> sumByRemainder(int divider, IntStream intStream) {
        Map<Integer, Integer> map = new HashMap<>();
        intStream.forEach(n -> {
            int key = n % divider;
            map.put(key, map.getOrDefault(key, 0) + n);
        });
        return map;
    }

    public Map<Person, Double> totalScores(Stream<CourseResult> results) {
        return results
                .collect(Collectors.toMap(
                    CourseResult::getPerson,
                    CourseResult::getTaskAverage));

    }

    public double averageTotalScore(Stream<CourseResult> results) {
        return results
                .map(CourseResult::getTaskAverage)
                .mapToDouble(n -> n)
                .average()
                .orElse(0);
    }

    public Map<String, Double> averageScoresPerTask(Stream<CourseResult> results) {
        List<CourseResult> resultList = results.collect(Collectors.toList());
        return resultList
                .stream()
                .flatMap(cr -> cr.getTaskResults().entrySet().stream())
                .collect(Collectors.groupingBy(
                        Map.Entry::getKey,
                        Collectors.summingDouble(Map.Entry::getValue)
                ))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue() / resultList.size()
                ));
    }

    public Map<Person, String> defineMarks(Stream<CourseResult> results) {
        return results
                .collect(Collectors.toMap(
                        CourseResult::getPerson,
                        CourseResult::getTaskMark
                ));
    }

    public String averageTotalMark(Stream<CourseResult> results) {
        return CourseResult.calculateMark(results
                .map(CourseResult::getTaskAverage)
                .mapToDouble(n -> n)
                .average()
                .orElse(0));
    }

    public String easiestTask(Stream<CourseResult> results) {
        return results
                .flatMap(cr -> cr.getTaskResults().entrySet().stream())
                .collect(Collectors.groupingBy(
                        Map.Entry::getKey,
                        Collectors.summingInt(Map.Entry::getValue)
                ))
                .entrySet()
                .stream()
                .max(Map.Entry.comparingByValue())
                .orElseThrow()
                .getKey();
    }

    public Collector<CourseResult, ?, String> printableStringCollector() {
        return new Collector<CourseResult, List<CourseResult>, String>() {
            @Override
            public Supplier<List<CourseResult>> supplier() {
                return ArrayList::new;
            }

            @Override
            public BiConsumer<List<CourseResult>, CourseResult> accumulator() {
                return List::add;
            }

            @Override
            public BinaryOperator<List<CourseResult>> combiner() {
                return (list1, list2) -> {
                    List<CourseResult> list = list1.size() > list2.size() ? list1 : list2;
                    list.addAll(list == list1 ? list2 : list1);
                    return list;
                };
            }

            @Override
            public Function<List<CourseResult>, String> finisher() {
                return (crList) -> {
                    List<String> programList = crList
                            .stream()
                            .flatMap(cr -> cr.getTaskResults().keySet().stream())
                            .distinct()
                            .sorted()
                            .collect(Collectors.toList());
                    ArrayList<ArrayList<String>> stringMatrix = crList
                            .stream()
                            .map(cr -> {
                                ArrayList<String> row = new ArrayList<>();
                                row.add(cr.getPerson().getLastName() + " " + cr.getPerson().getFirstName() + " ");
                                programList.forEach(programStr ->
                                        row.add(
                                                " " + cr.getTaskResults().getOrDefault(programStr, 0) + " "));
                                row.add(" " + SF.format(cr.getTaskAverage()) + " ");
                                row.add(" " + cr.getTaskMark() + " ");
                                return row;
                            })
                            .sorted(Comparator.comparing(strEle -> strEle.get(0)))
                            .collect(Collectors.toCollection(ArrayList::new));
                    stringMatrix.add(0 , printableStringCollectorHeader(programList));
                    stringMatrix.add(printableStringCollectorFooter(programList, crList));
                    printableStringCollectorFormatter(stringMatrix);
                    return stringMatrix
                            .stream()
                            .map(row -> String.join("|", row))
                            .collect(Collectors.joining("|\n")) + "|";
                };
            }

            @Override
            public Set<Characteristics> characteristics() {
                return Set.of(Characteristics.UNORDERED);
            }
        };
    }

    private static final DecimalFormat SF = new DecimalFormat("#,##0.00");
    static {
        SF.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
    }
    private static ArrayList<String> printableStringCollectorHeader(List<String> programList) {
        ArrayList<String> headerList = new ArrayList<>();
        headerList.add("Student ");
        programList.forEach(programStr ->
                headerList.add(" " + programStr + " "));
        headerList.add(" Total ");
        headerList.add(" Mark ");
        return headerList;
    }
    private static ArrayList<String> printableStringCollectorFooter(List<String> programList, List<CourseResult> crList) {
        ArrayList<String> headerList = new ArrayList<>();
        headerList.add("Average ");
        Map<String, Double> resultsByTask = new Collecting().averageScoresPerTask(crList.stream());
        programList.forEach(programStr ->
                headerList.add(
                        " " + SF.format(resultsByTask.get(programStr)) + " "));
        headerList.add(" " + SF.format(new Collecting().averageTotalScore(crList.stream())) + " ");
        headerList.add(" " + new Collecting().averageTotalMark(crList.stream()) + " ");
        return headerList;
    }

    private static void printableStringCollectorFormatter(ArrayList<ArrayList<String>> stringMatrix) {
        for (int j = 1; j < stringMatrix.size(); j++) {
            for (int i = 0; i < stringMatrix.get(j).size(); i++) {
                int diff = stringMatrix.get(j-1).get(i).length() - stringMatrix.get(j).get(i).length();
                if (diff > 0) {
                    String str = stringMatrix.get(j).get(i);
                    str = (i == 0) ? str + " ".repeat(diff) : " ".repeat(diff) + str;
                    stringMatrix.get(j).set(i, str);
                }
            }
        }
        for (int j = stringMatrix.size()-2; j >= 0; j--) {
            for (int i = 0; i < stringMatrix.get(j).size(); i++) {
                int diff = stringMatrix.get(j+1).get(i).length() - stringMatrix.get(j).get(i).length();
                if (diff > 0) {
                    String str = stringMatrix.get(j).get(i);
                    str = (i == 0) ? str + " ".repeat(diff) : " ".repeat(diff) + str;
                    stringMatrix.get(j).set(i, str);
                }
            }
        }
    }
}

