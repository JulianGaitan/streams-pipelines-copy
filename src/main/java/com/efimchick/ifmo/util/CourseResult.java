package com.efimchick.ifmo.util;

import java.util.Map;
import java.util.stream.Stream;

public class CourseResult {
    private final Person person;
    private final Map<String, Integer> taskResults;

    public CourseResult(final Person person, final Map<String, Integer> taskResults) {
        this.person = person;
        this.taskResults = taskResults;
    }

    public Person getPerson() {
        return person;
    }

    public Map<String, Integer> getTaskResults() {
        return taskResults;
    }

    public double getTaskAverage() {
        int total = taskResults.keySet().iterator().next().startsWith("Lab ") ? 3 : 4;
        Stream<Integer> streamValues = taskResults.values().stream();
        return streamValues.mapToDouble(n -> n).sum() / total;
    }

    public String getTaskMark() {
        return calculateMark(getTaskAverage());
    }

    public static String calculateMark(double avg) {
        return avg > 90 ? "A" : avg >= 83 ? "B" : avg >= 75 ? "C" : avg >= 68 ? "D" : avg >= 60 ? "E" : "F";
    }
}
